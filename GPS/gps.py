import os
import geocoder
import googlemaps
import gmaps
from geopy.geocoders import Nominatim
import requests
import json



# get my current location

def get_location(me, api_key):
    #print("Latitude: " + str(me.latlng[0]))
    #print("Longitude: " + str(me.latlng[1]))
    #print(' ')

    return str(me.latlng[0]), str(me.latlng[1])


def get_address(me, api_key):
    url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + str(me.latlng[0]) + ',' + str(me.latlng[1]) + '&key=' + str(api_key)
    r = requests.get(url)
    j = json.loads(r.text)
    if j["status"] == "OK":
        address = j["results"][0]['formatted_address']
        #print("Address:")
        #print(address)
        #print(" ")

        return address

def get_nearby(me, api_key):
    url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=' + str(me.latlng[0]) + ',' + str(me.latlng[1]) + '&radius=50&key=' + str(api_key)
    r = requests.get(url)
    j = json.loads(r.text)
    if j["status"] == "OK":
        for each in j["results"]:
            if each["name"] == "Johannesburg":
                continue
            return(each["name"])

if __name__ == '__main__':

    with open('apikey.txt') as f:
        api_key = f.readline()
        f.close()
    print("API key loaded successfully...")

    me = geocoder.ip('me')

    get_location(me, api_key)
