#! /usr/bin/env python
#  -*- coding: utf-8 -*-
#
# GUI module generated by PAGE version 4.21
#  in conjunction with Tcl version 8.6
#    Mar 07, 2019 10:47:01 AM SAST  platform: Linux

import sys

import gps
import os
import geocoder
import googlemaps
import gmaps
from geopy.geocoders import Nominatim
import requests
import json


try:
    import Tkinter as tk
except ImportError:
    import tkinter as tk

try:
    import ttk
    py3 = False
except ImportError:
    import tkinter.ttk as ttk
    py3 = True

#import gui_support

def vp_start_gui():
    '''Starting point when module is the main routine.'''
    global val, w, root
    root = tk.Tk()
    top = Toplevel1 (root)
    #gui_support.init(root, top)
    root.mainloop()

w = None
def create_Toplevel1(root, *args, **kwargs):
    '''Starting point when module is imported by another program.'''
    global w, w_win, rt
    rt = root
    w = tk.Toplevel (root)
    top = Toplevel1 (w)
    #gui_support.init(w, top, *args, **kwargs)
    return (w, top)

def destroy_Toplevel1():
    global w
    w.destroy()
    w = None

class Toplevel1:
    def __init__(self, top=None):
        '''This class configures and populates the toplevel window.
           top is the toplevel containing window.'''
        _bgcolor = '#d9d9d9'  # X11 color: 'gray85'
        _fgcolor = '#000000'  # X11 color: 'black'
        _compcolor = '#d9d9d9' # X11 color: 'gray85'
        _ana1color = '#d9d9d9' # X11 color: 'gray85'
        _ana2color = '#ececec' # Closest X11 color: 'gray92'

        top.geometry("611x301+440+54")
        top.title("New Toplevel")
        top.configure(highlightcolor="black")

        self.Canvas1 = tk.Canvas(top)
        self.Canvas1.place(relx=0.0, rely=0.0, relheight=1.0, relwidth=1.0)
        self.Canvas1.configure(borderwidth="2")
        self.Canvas1.configure(relief='ridge')
        self.Canvas1.configure(selectbackground="#c4c4c4")
        self.Canvas1.configure(width=611)

        self.lblLat = tk.Label(self.Canvas1)
        self.lblLat.place(relx=0.049, rely=0.066, height=21, width=61)
        self.lblLat.configure(activebackground="#f9f9f9")
        self.lblLat.configure(text='''Latitude:''')

        self.outLat = tk.Label(self.Canvas1)
        self.outLat.place(relx=0.196, rely=0.066, height=15, width=359)
        self.outLat.configure(activebackground="#f9f9f9")
        self.outLat.configure(text='''...''')

        self.lblLong = tk.Label(self.Canvas1)
        self.lblLong.place(relx=0.049, rely=0.166, height=21, width=72)
        self.lblLong.configure(activebackground="#f9f9f9")
        self.lblLong.configure(text='''Longitude:''')

        self.outLong = tk.Label(self.Canvas1)
        self.outLong.place(relx=0.196, rely=0.133, height=15, width=355)
        self.outLong.configure(activebackground="#f9f9f9")
        self.outLong.configure(text='''...''')

        self.lblAddress = tk.Label(self.Canvas1)
        self.lblAddress.place(relx=0.049, rely=0.365, height=21, width=60)
        self.lblAddress.configure(activebackground="#f9f9f9")
        self.lblAddress.configure(text='''Address:''')

        self.outAddress = tk.Label(self.Canvas1)
        self.outAddress.place(relx=0.18, rely=0.365, height=15, width=479)
        self.outAddress.configure(activebackground="#f9f9f9")
        self.outAddress.configure(text='''...''')

        self.lblNearby = tk.Label(self.Canvas1)
        self.lblNearby.place(relx=0.049, rely=0.532, height=21, width=55)
        self.lblNearby.configure(activebackground="#f9f9f9")
        self.lblNearby.configure(text='''Nearby:''')

        self.outNearby = tk.Label(self.Canvas1)
        self.outNearby.place(relx=0.213, rely=0.532, height=21, width=439)
        self.outNearby.configure(activebackground="#f9f9f9")
        self.outNearby.configure(text='''...''')

        def refresh():
            try:
                self.outLat.configure(text=gps.get_location(me,api_key)[0])
            except:
                self.outLat.configure(text="Error displaying location")
            try:
                self.outLong.configure(text=gps.get_location(me,api_key)[1])
            except:
                self.outLong.configure(text="Error displaying location")
            try:
                self.outAddress.configure(text=gps.get_address(me, api_key))
            except:
                self.outAddress.configure(text="Error displaying address")
            try:
                self.outNearby.configure(text=gps.get_nearby(me, api_key))
            except:
                self.outNearby.configure(text="Error displaying nearby")

        
        self.btnRefresh = tk.Button(self.Canvas1)
        self.btnRefresh.place(relx=0.409, rely=0.764, height=31, width=78)
        self.btnRefresh.configure(command=refresh)
        self.btnRefresh.configure(text='''Refresh''')

if __name__ == '__main__':

    with open('apikey.txt') as f:
        api_key = f.readline()
        f.close()
    print("API key loaded successfully...")

    me = geocoder.ip('me')
    vp_start_gui()





